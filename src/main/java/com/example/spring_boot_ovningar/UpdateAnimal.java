package com.example.spring_boot_ovningar;

import lombok.Value;

@Value
public class UpdateAnimal {
    String name;
    String binomialName;
}
