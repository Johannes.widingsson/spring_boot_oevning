package com.example.spring_boot_ovningar;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AnimalEntity {
    String id;
    String name;
    String binomialName;
    String Description;
    String ConservationStatus;
}
