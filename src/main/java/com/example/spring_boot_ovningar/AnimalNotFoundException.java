package com.example.spring_boot_ovningar;

public class AnimalNotFoundException extends Exception{
    public AnimalNotFoundException(String id) {
        super(id);
    }
}
