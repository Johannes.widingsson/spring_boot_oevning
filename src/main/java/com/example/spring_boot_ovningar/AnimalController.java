package com.example.spring_boot_ovningar;

import com.example.spring_boot_ovningar.Animal;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/animals")
@AllArgsConstructor

public class AnimalController {

    AnimalServices animalServices;


    @GetMapping
    public List<Animal> all() {
        return animalServices.all().map(AnimalController::toDTO).collect(Collectors.toList());

    }


    @PostMapping
    public Animal createAnimal(@RequestBody CreateAnimal createAnimal) {
        return toDTO(animalServices.creatAnimal(createAnimal.getName(), createAnimal.getBinomialName()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Animal> get(@PathVariable("id") String id) {
        try {
            return ResponseEntity.ok(toDTO(animalServices.get(id)));
        } catch (AnimalNotFoundException e) {
           return ResponseEntity.notFound().build();
        }

    }
@GetMapping("/{id}/Link/{remoteId}")
    public ResponseEntity<Animal> Link(@PathVariable("id") String id, @PathVariable("remoteId") String remoteId) {
        try {
            return ResponseEntity.ok(toDTO(animalServices.Link(id, remoteId)));
        } catch (AnimalNotFoundException e) {
           return ResponseEntity.notFound().build();
        }

    }

    @PutMapping("/{id}")
    public ResponseEntity<Animal>  update(@PathVariable("id") String id, @RequestBody UpdateAnimal updateAnimal) {
        try {
            return ResponseEntity.ok(toDTO(animalServices.updateAnimal(id,updateAnimal.getName(), updateAnimal.getBinomialName())));
        } catch (AnimalNotFoundException e) {
            return ResponseEntity.notFound().build();
        }

    }

    @DeleteMapping("/{id}")
    public ResponseEntity <Void> delete(@PathVariable("id") String id) {
        try {
            animalServices.delete(id);
            return ResponseEntity.ok().build();
        } catch (AnimalNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    private static Animal toDTO(AnimalEntity animalEntity) {
        return new Animal(
                animalEntity.getId(),
                animalEntity.getName(),
                animalEntity.getBinomialName(),
                animalEntity.getDescription(),
                animalEntity.getConservationStatus()

        );
    }
}
