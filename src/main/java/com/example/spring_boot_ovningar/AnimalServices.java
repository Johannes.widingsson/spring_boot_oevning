package com.example.spring_boot_ovningar;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class AnimalServices {

    AnimalRepository animalRepository;
    JsonPlaceholderRemote jsonPlaceholderRemote;



    public Stream<AnimalEntity> all() {
        return animalRepository.all();
    }



    public AnimalEntity creatAnimal(String name, String binomialName) {


       AnimalEntity animalEntity = new AnimalEntity(
                UUID.randomUUID().toString(),
                name,
                binomialName,
                null,
                null

        );
        return animalRepository.save(animalEntity);
    }

    public AnimalEntity get(String id) throws AnimalNotFoundException {
        return animalRepository.get(id)
                .orElseThrow(() -> new AnimalNotFoundException(id));
    }

    public AnimalEntity updateAnimal(String id, String name, String binomialName) throws AnimalNotFoundException {
        AnimalEntity animalEntity = animalRepository.get(id)
                .orElseThrow(() -> new AnimalNotFoundException(id));
        animalEntity.setBinomialName(name);
        animalEntity.setBinomialName(binomialName);
        return animalRepository.save(animalEntity);
    }

    public void delete(String id) throws AnimalNotFoundException {
        AnimalEntity animalEntity = animalRepository.get(id)
                .orElseThrow(() -> new AnimalNotFoundException(id));
        animalRepository.delete(animalEntity);

    }

    public AnimalEntity Link(String id, String remoteId) throws AnimalNotFoundException {
        AnimalEntity animalEntity = animalRepository.get(id)
                .orElseThrow(() -> new AnimalNotFoundException(id));
        JsonPlaceholderRemote.JsonPlaceholder Json = jsonPlaceholderRemote.get(remoteId);
        animalEntity.setDescription(Json.getBody());
        return animalRepository.save(animalEntity);

    }
}
